from Asphere import AsphericSurface
from Ray import Ray
import numpy as np
import matplotlib.pylab as plt

lens_aperture_radius=5
asf = AsphericSurface(curvature=1./20, konic=0.0,z_0=0., A=1.e-5,B=1.e-8,
                aperture_radius=lens_aperture_radius, material_nr=2.0)
bsf = AsphericSurface(curvature=-1./20, konic=0.0,z_0=15, A=0.0,B=0.0,
                aperture_radius=lens_aperture_radius, material_nr=1.0)

ray_number=11
y_start = np.linspace(-lens_aperture_radius*0.8, lens_aperture_radius*0.8,
                      num=ray_number)
x_start = np.zeros_like(y_start)
z_start = np.ones_like(y_start)-20.
theta = np.zeros_like(y_start)+0.
phi = np.zeros_like(y_start)+0.
rays = []

fig,ax=plt.subplots()
asf.render(ax)
tangent_vec_length=2.0
y_pos_array = np.linspace(-lens_aperture_radius*0.9, lens_aperture_radius*0.9,
                      num=ray_number)
x_pos_array = np.zeros_like(y_pos_array)

for xp,yp in zip(x_pos_array,y_pos_array):
    # get tangent vector
    n_v=asf.get_norm_vec(xp,yp)
    n_v=n_v*2
    zp=asf.get_surface_zr(xp,yp)
    
    # plot normal vectors
    ax.plot([zp-n_v[2],zp+n_v[2]],[yp-n_v[1],yp+n_v[1]],color="k")

ax.set_xlim(left=-5,right=5)
plt.show()


fig,ax=plt.subplots()
asf.render(ax)
bsf.render(ax)
for i in range(y_start.shape[0]):
    ray_wavelength=500
    ray = Ray(x_start[i], y_start[i], z_start[i], theta[i], phi[i], ray_wavelength)
    rays.append(ray)

    asf.intersect(ray, t_min=0, t_max=100, prev_n=None)
    bsf.intersect(ray, t_min=0, t_max=100, prev_n=asf.n_r)
    ray.render_all(ax,time_of_flights=50)
plt.show()

