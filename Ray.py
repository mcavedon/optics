from scipy.interpolate import interp1d
from matplotlib.axes import Axes
import matplotlib.pylab as plt
from typing import Union,Callable
import os
import math
import numpy as np

def _init_vec(x,y,z):
    vec=np.empty(3)
    vec[0]=x
    vec[1]=y
    vec[2]=z
    return vec

def _fast_norm(x, y, z):
    return math.sqrt(math.pow(x,2)+math.pow(y,2)+math.pow(z,2))

class Ray(object):
    """
    The ray is a set of line segments. Each segment is represented by a vector v=[x,y,z]
    Ray-tracing problem also requires parameterized expressions, i.e.,
    x(t)=x_0+sin(theta)*cos(phi)t
    y(t)=y_0+sin(theta)*sin(phi)*t
    z(t)=z_0+cos(theta)*t

    x_0, y_0, z_0, theta are updated after an intersection with a surface occurs.
    Lists are used to store the old values of x_0, y_0, z_0, theta and phi so that a figure renderer can plot all the segments


    The ray data structure is:

        |                                  |
        |                                  |
        |                                  |
        |                                  |
    starting point                      surface 1

       t_0                              t_1=
                                        t_end+t_0

     self.path: [v0,theta_0,phi_0]      [[v_0,theta_0,phi_0],[v_1,theta_1,phi_1]]
     self.end_ts: [Inf]                 [t_1,Inf]
     theta_x is raw angle from surface X, x=0 is starting point.
    v_0 are the turning points of the ray vector. Every time a ray is intersected with a surface, the value of v_0 is updated
    t_end is the ending "time" t of each line segment

     Conventions of the coordinate:
     z(right), y(up), x(forward). Same as the coordinate used in ZEMAX or other optics textbook


    """

    def __init__(self, x_0, y_0, z_0, theta, phi, wavelength=600):
        """
        Initialize a beam

        :param x_0: starting x position
        :param y_0: starting y position
        :param z_0: starting z position
        :param theta: traveling angle (theta)
        :param theta: traveling angle (phi)
        :param wavelength: the wavelength of a ray in nanometer (not used in this implementation yet)
        """
        self.x_0 = x_0
        self.y_0 = y_0
        self.z_0 = z_0
        self.theta = theta
        self.phi = phi
        self.dt = 0
        self.v_0 = _init_vec(self.x_0, self.y_0, self.z_0)  # vector that stores the initial point of the ray
        self.paths = [[np.copy(self.v_0), np.copy(self.theta), np.copy(self.phi)]]
        self.end_ts = [float('Inf')]
        self.k = _init_vec(np.sin(self.theta)*np.cos(self.phi), np.sin(self.theta)*np.sin(self.phi), np.cos(self.theta))
        self.intensity = 1
        self.wavelength = wavelength

    def update_after_intersect(self, t_end, new_theta, new_phi, end_beam=False):
        """
        Update the state of a ray, including:
        - a new starting point: [z_0,y_0]
        - angle of directional cosine: theta
        - angle of directional cosine: phi

        :param t_end:
        :param new_theta: new traveling angle theta (radians)
        :param attenu_coef: attenuation coefficient, assigned by the Surface object
        :param end_beam: True if the ray will be stopped at this surface
        :return: None
        """

        self.v_0 += self.k * t_end

        self.x_0 = self.v_0[0]
        self.y_0 = self.v_0[1]
        self.z_0 = self.v_0[2]

        self.update_angles(new_theta, new_phi)
        next_t = t_end + self.dt
        self.dt = next_t
        self.end_ts[-1] = next_t

        self.paths.append([np.copy(self.v_0), np.copy(self.theta), np.copy(self.phi)])

        if not end_beam:
            self.end_ts.append(float('Inf'))

    def estimate_t_from_z(self, zp:float):

        t = (zp - self.v_0[2]) / self.k[2]

        return t


    def get_xyz(self, delta_t):
        """
        Get x, y and z positions from parameter t

        :param delta_t:
        :return: np.array [z,y]
        """
        vv = self.v_0 + self.k * delta_t
        return vv[0], vv[1], vv[2]

    def update_angles(self, new_theta, new_phi):
        """
        Update the traveling angle theta

        :param new_theta: new traveling angle theta of the ray
        :return: None
        """
        self.theta = new_theta
        self.phi = new_phi
        self.k = _init_vec(np.sin(self.theta)*np.cos(self.phi), np.sin(self.theta)*np.sin(self.phi), np.cos(self.theta))

    def render(self, ax: Axes, time_of_fights, color='C0'):
        """
        Render the ray start from the most recent reracted surface

        :param ax: Matplotlib Axes to plot on
        :param time_of_fights: the stopping time that the beam ends
        :param color: matplotlib color
        :return: None
        """
        v_e = self.v_0 + time_of_fights * self.k

        v_for_plots = np.vstack((self.v_0, v_e))
        xs = v_for_plots[:, 1]
        ys = v_for_plots[:, 2]

        ax.plot(xs, ys, color=color)

    def get_k_from_theta_phi(self, theta, phi):

        k = _init_vec(np.sin(theta)*np.cos(phi), np.sin(theta)*np.sin(phi), np.cos(theta))

        return k

    def render_all(self, ax, time_of_flights, color=None):
        """
        Plot all rays on the axes.

        :param ax: axes to be plotted on
        :param time_of_flights: end travel time of the ray
        :param color: matplotlib color, such as 'C0', 'C1' or 'blue', 'red'. Set None for automatic colors.
        :return:
        """

        prev_t = 0
        for idx in range(len(self.end_ts)):
            v_0, theta, phi = self.paths[idx]
            end_t = self.end_ts[idx]
            k = self.get_k_from_theta_phi(theta,phi)

            if time_of_flights > end_t:
                v_e = v_0 + (end_t - prev_t) * k
            else:
                v_e = v_0 + (time_of_flights - prev_t) * k
            v_for_plots = np.vstack((v_0, v_e))
            ys = v_for_plots[:, 1]
            zs = v_for_plots[:, 2]
            prev_t = end_t
            ax.plot(zs, ys, linewidth=0.5)

if __name__=="__main__":
    r = Ray(0,0,1,0.1,0.1)
    f,a = plt.subplots()
    r.render(a,50)
    plt.show()



