from scipy.optimize import newton
from math import *
from Ray import *
import numpy as np

# 3D extension of https://www.kaggle.com/photunix/ray-tracing-in-python
from sympy import sin, cos, sqrt, power, symbols, diff, lambdify

# Use sympy do derive expression for an aspheric lens
# Define variables
z_sy,y_sy,x_sy,r_sy,c_sy,k_sy,zl_sy,A_sy,B_sy,R_c_sy,zinit_sy = \
        symbols('z_sy y_sy x_sy r_sy c_sy k_sy zl_sy A_sy B_sy R_c_sy zinit_sy')
r_sy = sqrt(x_sy**2+y_sy**2)

# Aspheric surface as a function to the distance from the center of the lens
spherical_expr = c_sy*r_sy**2/(1+sqrt(1-(1+k_sy)*(c_sy*r_sy)**2))+A_sy*r_sy**4+B_sy*r_sy**6+zinit_sy-z_sy

# Create function returnig numerical expression imposing z_sy = 0
surface_zr = lambdify([x_sy,y_sy,c_sy,zinit_sy,k_sy,A_sy,B_sy],spherical_expr.subs(z_sy,0),"numpy")

# First Derivative over dzdx and dzdy to determine tangent vector
spherical_expr_yprime=diff(spherical_expr.subs(z_sy,0),y_sy)
spherical_expr_xprime=diff(spherical_expr.subs(z_sy,0),x_sy)
dspherical_expr_dy = lambdify([x_sy,y_sy,c_sy,k_sy,A_sy,B_sy],spherical_expr_yprime,"numpy")
dspherical_expr_dx = lambdify([x_sy,y_sy,c_sy,k_sy,A_sy,B_sy],spherical_expr_xprime,"numpy")

# Find Parametric expression in cylindrical coords
theta_sy,phi_sy,t_sy=symbols('theta_sy phi_sy t_sy')
z0_sy,y0_sy,x0_sy=symbols('z0_sy y0_sy x0_sy')
ray_x_sy = x0_sy+sin(theta_sy)*cos(phi_sy)*t_sy
ray_y_sy = y0_sy+sin(theta_sy)*sin(phi_sy)*t_sy
ray_z_sy = z0_sy+cos(theta_sy)*t_sy

# Substitute into the spherical_expr
param_spherical_expr = spherical_expr.subs(y_sy,ray_y_sy).subs(x_sy,ray_x_sy).subs(z_sy,ray_z_sy)
param_spherical_expr_lambda = lambdify([t_sy, c_sy, k_sy, x0_sy, y0_sy, z0_sy, theta_sy, phi_sy, A_sy, B_sy, zinit_sy],\
        param_spherical_expr,"numpy")
param_spherical_expr_prime=diff(param_spherical_expr,t_sy)
param_spherical_expr_dt_lambda = lambdify([t_sy, c_sy, k_sy, x0_sy, y0_sy, z0_sy, theta_sy, phi_sy, A_sy, B_sy],\
        param_spherical_expr_prime,"numpy")
param_spherical_expr_prime2=diff(param_spherical_expr,t_sy)
param_spherical_expr_dt2_lambda = lambdify([t_sy, c_sy, k_sy, x0_sy, y0_sy, z0_sy, theta_sy, phi_sy, A_sy, B_sy],\
        param_spherical_expr_prime,"numpy")




def _fast_norm(x, y, z):
    return math.sqrt(math.pow(x,2)+math.pow(y,2)+math.pow(z,2))

class AsphericSurface(object):
    def __init__(self, curvature, z_0, konic, aperture_radius=10, A=0, B=0,
                 record_rays=False, material_nr=1.0, end_beam=False):
        """
        Aspheric surface object.


        :param curvature: curvature of the lens in (mm^-1)
        :param z_0: center position of the lens on z-axis
        :param konic: conic coefficient
        :param aperture_radius: radius of the aperture
        :param A: fourth order coefficient of the lens
        :param B: sixth order coefficient of the lens
        :param record_rays: True if the surface is a detector that records the ray that hit the surface
        :param attenu_coef: attenuation coefficient  (1/mm)
        """
        self.c = curvature
        self.k = konic
        self.z_0 = z_0
        self.A = A
        self.B = B
        self.aperture_radius = aperture_radius
        self.ray_bins = []
        self.record_rays = record_rays

        # FUTURE improvement: this is temporary. Future version may have shifts in y
        self.y_min = -aperture_radius
        self.y_max = aperture_radius

        self.constant_nr = material_nr
        self.n_r = self._constant_nr

        self.end_beam = end_beam

    def _constant_nr(self, wavelength):
        """
        function of wavelength dependent refractive index n_r(labmda)

        :param wavelength: can be anything
        :return: preset refractive index
        """

        return self.constant_nr

    def get_surface_zr(self, x, y):
        z = surface_zr(x, y, self.c, self.z_0, self.k, self.A, self.B)
        return z

    def spherical_lens_dzdy(self, x, y, c, k, A, B):
        return dspherical_expr_dy(x, y, c, k, A, B)

    def spherical_lens_dzdx(self, x, y, c, k, A, B):
        return dspherical_expr_dx(x, y, c, k, A, B)

    def ray_param_eq(self, t, c, k, x0, y0, z0, theta, phi, A, B):
        return param_spherical_expr_lambda(t, c, k, x0, y0, z0, theta, phi, A, B, self.z_0)

    def ray_param_eq_prime(self, t, c, k, x0, y0, z0, theta, phi, A, B):
        return param_spherical_expr_dt_lambda(t, c, k, x0, y0, z0, theta, phi, A, B)

    def ray_param_eq_prime2(self, t, c, k, x0, y0, z0, theta, phi, A, B):
        return param_spherical_expr_dt2_lambda(t, c, k, x0, y0, z0, theta, phi, A, B)

    def add_rays_into_bins(self, x, y, z, intensity, wavelength):
        self.ray_bins.append((x, y, z, intensity, wavelength))

    def get_norm_vec(self, xp, yp):
        """
        Calculate the normal vector of at the position x,y

        :param xp:
        :param yp:
        :return: a 3D np.array([x,y,z])

        https://en.wikipedia.org/wiki/Normal_(geometry)#Normal_to_surfaces_in_3D_space
        """
        dzdx_p = self.spherical_lens_dzdx(xp, yp, self.c, self.k, self.A, self.B)
        dzdy_p = self.spherical_lens_dzdy(xp, yp, self.c, self.k, self.A, self.B)
        normal_vec = np.empty(3)
        normal_vec[0] = +dzdx_p
        normal_vec[1] = +dzdy_p
        normal_vec[2] = -1
        normal_vec = normal_vec / _fast_norm(normal_vec[0], normal_vec[1], normal_vec[2])

        return normal_vec

    def get_refraction(self, xp, yp, ray: Ray, prev_n: float,plot=False):
        #https://physics.stackexchange.com/questions/435512/snells-law-in-vector-form
        n_r = self.n_r(ray.wavelength)
        norm_vec = -self.get_norm_vec(xp,yp)

        mu = prev_n/n_r
        next_r = np.sqrt(1-mu**2*(1-np.dot(norm_vec,ray.k)**2))*norm_vec + \
                mu*(ray.k-np.dot(norm_vec,ray.k)*norm_vec)
        if plot:
            f,ax = plt.subplots()
            ax.set_aspect('equal')
            zp=self.get_surface_zr(xp,yp)
            ax.plot([zp-ray.k[2],zp+ray.k[2]],[yp-ray.k[1],yp+ray.k[1]],color="r")
            ax.plot([zp-next_r[2],zp+next_r[2]],[yp-next_r[1],yp+next_r[1]],color="k")
            ax.plot([zp-norm_vec[2],zp+norm_vec[2]],[yp-norm_vec[1],yp+norm_vec[1]],color="b")
            self.render(ax)
            plt.show()
        return next_r

    def intersect(self, ray: Ray, prev_n: Union[Callable[[float], float], None],
                  t_min=0, t_max=10):

        def unity_func(x):
            return 1.0

        if prev_n is None:
            prev_n = unity_func

        # Make a initial guess of t for newton-raphson solver
        # the t could be either at the edge of a surface or the center of a surface
        # depending on its shape
        t_min_p_1 = ray.estimate_t_from_z(self.get_surface_zr(self.aperture_radius,self.aperture_radius))
        t_min_p_2 = ray.estimate_t_from_z(self.z_0)
        t_min_p = min(t_min_p_1, t_min_p_2)

        t_end = newton(self.ray_param_eq, (t_min_p + t_max) / 2,
                   fprime=self.ray_param_eq_prime,
                   fprime2=self.ray_param_eq_prime2,
                   args=(self.c, self.k, ray.x_0, ray.y_0, ray.z_0, ray.theta, ray.phi, self.A, self.B))
        ## Calculate the intersected point between the ray and the surface
        x_end, y_end, z_end = ray.get_xyz(t_end)
        

        ## We have to check the point of intersection is within the boundary
        if (y_end <= self.y_max) and (y_end >= self.y_min):
            if self.record_rays:
                self.add_rays_into_bins(x_end, y_end, z_end, ray.intensity, ray.wavelength)

            next_r = self.get_refraction(x_end, y_end, ray, prev_n=prev_n(ray.wavelength))
            new_phi = np.arctan2(next_r[1], next_r[0])
            new_theta = np.arccos(next_r[2]/_fast_norm(next_r[0],next_r[1],next_r[2]))

            ray.update_after_intersect(t_end, new_theta=new_theta, new_phi=new_phi,
                                       end_beam=self.end_beam)

    def render(self, ax, point_num=1200):
        y = np.linspace(-self.aperture_radius, self.aperture_radius, point_num)
        x = y*0
        z = self.get_surface_zr(x,y)

        ax.plot( z, y, color='black', linewidth=0.5)
        ax.set_xlabel("z-position")
        ax.set_ylabel("y-position")
